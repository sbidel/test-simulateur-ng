import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-simulateur',
  templateUrl: './simulateur.component.html',
  styleUrls: ['./simulateur.component.css']
})
export class SimulateurComponent implements OnInit {
  montant: number;
  duree: number;
  mensualite: number;
  coutCredit: number;
  taux = 0.045;

  constructor() {
  }

  ngOnInit() {
  }

  simule(): void {
    //attention calcul faux : juste pour tester
    this.mensualite = this.montant * (1 + this.taux) / this.duree;
    this.coutCredit = this.taux * this.montant;
  }

}
